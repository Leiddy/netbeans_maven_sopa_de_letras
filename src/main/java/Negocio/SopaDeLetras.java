package Negocio;

import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.*;

/**
 * Write a description of class SopaDeLetras here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class SopaDeLetras {

    // instance variables - replace the example below with your own
    private char sopas[][];

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras() {

    }

    public SopaDeLetras(String palabras) throws Exception {
        if (palabras == null || palabras.isEmpty()) {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }

        //Crear la matriz con las correspondientes filas:
        String palabras2[] = palabras.split(",");
        this.sopas = new char[palabras2.length][];
        int i = 0;
        for (String palabraX : palabras2) {
            //Creando las columnas de la fila i
            this.sopas[i] = new char[palabraX.length()];
            pasar(palabraX, this.sopas[i]);
            i++;

        }

    }

    private void pasar(String palabra, char fila[]) {

        for (int j = 0; j < palabra.length(); j++) {
            fila[j] = palabra.charAt(j);
        }
    }

    public String toString() {
        String msg = "";
        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < this.sopas[i].length; j++) {
                msg += this.sopas[i][j] + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public String toString2() {
        String msg = "";
        for (char filas[] : this.sopas) {
            for (char dato : filas) {
                msg += dato + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public boolean getDiag() {
        return false;
    }

    public boolean esCuadrada() {
        if (this.sopas.length == this.sopas[0].length && !esDispersa()) {
            return true;
        }
        return false;
    }

    public boolean esDispersa() {
        int tamaño = this.sopas[0].length;
        for (int i = 1; i < this.sopas.length; i++) {
            if (this.sopas[i].length != tamaño) {
                return true;
            }
        }
        return false;
    }

    public boolean esRectangular() {
        if (!esDispersa() && this.sopas.length != this.sopas[0].length) {
            return true;
        }
        return false;
    }

    /*
     retorna cuantas veces esta la palabra en la matriz
     */
    public int getContar(String palabra) {
        return 0;
    }

    /*
     debe ser cuadrada sopas
     */
    public char[] getDiagonalPrincipal() throws Exception {
        return null;
    }

    public void leerExcel() throws Exception {
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream("Libro.xls"));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum() + 1;
        String[][] a = new String[canFilas][];
        sopas = new char[canFilas][];

        System.out.println("Filas:" + canFilas);

        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila

            int cantCol = filas.getLastCellNum();
            a[i] = new String[cantCol];
            sopas[i] = new char[cantCol];
            for (int j = 0; j < cantCol; j++) {
                //Obtiene la celda y su valor respectivo
                //double r=filas.getCell(i).getNumericCellValue();
                String valor = filas.getCell(j).getStringCellValue();
                a[i][j] = filas.getCell(j).getStringCellValue();
                System.out.print(valor + "\t");
            }

            System.out.println();
        }
        for (int k = 0; k < a.length; k++) {
            for (int o = 0; o < a[k].length; o++) {
                sopas[k][o] = a[k][o].charAt(0);
            }
        }
    }

    public int getDerechaIzquierda() {
        String palabra = "AZUL";
        String m = "";
        byte a = 0;
        byte numeroDePalabras = 0;
        for (int i = 0; i < sopas.length; i++) {
            m = "";
            a = 0;
            for (int j = sopas[i].length - 1; j > -1; j--) {
                if (sopas[i][j] == palabra.charAt(a)) {
                    a++;
                    if (a == palabra.length()) {
                        a = 0;
                    }
                    m = m + sopas[i][j];
                    if (palabra.equals(m)) {
                        numeroDePalabras++;
                        m = "";
                        a = 0;
                    }
                } else {
                    m = "";
                    a = 0;
                }
            }
        }

        return numeroDePalabras;
    }

    public int getIzquierdaDerecha() {
        String palabra = "AZUL";
        String m = "";
        byte a = 0;
        byte numeroDePalabras = 0;
        for (int i = 0; i < sopas.length; i++) {
            m = "";
            a = 0;
            for (int j = 0; j < sopas[i].length; j++) {
                if (sopas[i][j] == palabra.charAt(a)) {
                    a++;
                    if (a == palabra.length()) {
                        a = 0;
                    }
                    m = m + sopas[i][j];
                    if (palabra.equals(m)) {
                        numeroDePalabras++;
                        m = "";
                        a = 0;
                    }
                } else {
                    m = "";
                    a = 0;
                }
            }
        }
        return numeroDePalabras;
    }

    public int getArribaAbajo() {
        String palabra = "AZUL";
        String m = "";
        byte a = 0;
        byte numeroDePalabras = 0;
        for (int i = 0; i < sopas[i].length; i++) {
            m = "";
            a = 0;
            for (int j = 0; j < sopas.length; j++) {
                if (sopas[j][i] == palabra.charAt(a)) {
                    a++;
                    if (a == palabra.length()) {
                        a = 0;
                    }
                    m = m + sopas[j][i];
                    if (palabra.equals(m)) {
                        numeroDePalabras++;
                        m = "";
                        a = 0;
                    }
                } else {
                    m = "";
                    a = 0;
                }
            }
        }
        return numeroDePalabras;
    }

    public int getAbajoArriba() {
        String palabra = "AZUL";
        String m = "";
        byte a = 0;
        byte numeroDePalabras = 0;
        for (int i = 0; i < sopas[i].length; i++) {
            m = "";
            a = 0;
            for (int j = sopas.length - 1; j > -1; j--) {
                if (sopas[j][i] == palabra.charAt(a)) {
                    a++;
                    if (a == palabra.length()) {
                        a = 0;
                    }
                    m = m + sopas[j][i];
                    if (palabra.equals(m)) {
                        numeroDePalabras++;
                        m = "";
                        a = 0;
                    }
                } else {
                    m = "";
                    a = 0;
                }
            }
        }
        return numeroDePalabras;
    }

    //Start GetterSetterExtension Source Code
    /**
     * GET Method Propertie sopas
     */
    public char[][] getSopas() {
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!
}
